import firebase from "firebase/app"
import "firebase/auth"

const app = firebase.initializeApp({
  apiKey: "AIzaSyB7sSPq-QsJ-TseURWW-9_ViX_9hLCmpyc",
  authDomain: "melayulearning.firebaseapp.com",
  databaseURL: "https://melayulearning-default-rtdb.firebaseio.com",
  projectId: "melayulearning",
  storageBucket: "melayulearning.appspot.com",
  messagingSenderId: "121163430427",
  appId: "1:121163430427:web:1ed09f6d5f9553b72216be",
  measurementId: "G-VPKM4MFPJ7"
})

export const auth = app.auth()
export default app
