import React, { useEffect, useState } from "react";
import { Redirect, useParams, useHistory } from "react-router";
import firebase from "firebase";
import { useAuth } from "../Contexts/AuthContext"
//use css makeStyle
import { makeStyles } from '@material-ui/core/styles';
//Material
import { Container, Grid, Button, Paper } from '@material-ui/core';
import { MicRounded, SettingsVoiceRounded, RecordVoiceOverRounded } from '@material-ui/icons';

import Lottie from "react-lottie";
//component
import * as loadingData from "../loading.json";

import Progressbar from "../Component/Progressbar";
import Audios from "../Component/Audios";
import Image from "../Component/Image";

///Library other
import { ReactMic } from "react-mic";
import swal from 'sweetalert';
import axios from 'axios';
import md5 from 'md5';

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loadingData.default,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
    }
};

function LearnMode1({ lessonDb = '../LessonDB/DB_introduction.json' }) {
    const REST_AUDIO_CHECK_SERVER = "http://127.0.0.1:8002/eval_audio/"
    const { currentUser } = useAuth()

    const classes = useStyles();
    const history = useHistory();

    const id = useParams()
    const lessonDbID = id.lessonDbID

    const [preLoading, setPreloading] = useState(true)

    //jsonDB
    let wordDB;
    if (lessonDbID == 1)
        wordDB = require('../LessonDB/DB_introduction.json');
    else if (lessonDbID == 2)
        wordDB = require('../LessonDB/DB_traveling.json');
    else if (lessonDbID == 3)
        wordDB = require('../LessonDB/lessonDB.json');

    const [category, setCategory] = useState("");
    const [word, setWord] = useState("");
    const [meaningLabel, setMeaningLabel] = useState("");
    const [audioName, setAudioName] = useState("");
    const [audioPath, setAudioPath] = useState("");
    const [imageFileName, setImageFileName] = useState("");
    const [syllable, setSyllable] = useState(0);

    //next step
    const [nextStep, setNextStep] = useState(0);
    const [win, setWin] = useState([]);

    useEffect(() => {
        setCategory(wordDB[nextStep].category);
        setWord(wordDB[nextStep].word);
        setMeaningLabel(wordDB[nextStep].meaning);
        setAudioName(wordDB[nextStep].audioName);
        setAudioPath(wordDB[nextStep].audioPath);
        setImageFileName(wordDB[nextStep].imageName);
        setSyllable(wordDB[nextStep].syllable);
    });

    useEffect(() => {
        upadateData()
    }, []);

    const upadateData = async () => {

        //get data nextlevel
        var userId = /*firebase.auth().*/currentUser.uid;
        await firebase.database().ref('/users/' + userId + '/successLearn/lesson' + lessonDbID).once('value').then((snapshot) => {
            var step = (snapshot.val() && snapshot.val().step) || 0;
            setNextStep(step)

        });
        //get data Lesson success 
        await firebase.database().ref('/users/' + userId + '/successLearn/lesson' + lessonDbID).once('value').then((snapshot) => {
            var win = (snapshot.val() && snapshot.val().win) || 0;
            setWin(win);
        })
        //set preload false
        setPreloading(false)


    }

    const previous = () => {
        if (nextStep > 0) {
            setNextStep(nextStep - 1)
            setRecored(false);
            setEverClickRecord(false);
        }
    }


    /***************************************** Mic ***************************************/
    const [record, setRecored] = useState(false);
    const [blob, setBlob] = useState("");
    const [blobURL, setBlobURL] = useState("");

    const [everClickPlaySound, setEverClickPlay] = useState(true)
    const [everClickRecord, setEverClickRecord] = useState(false)

    //const [blobMD5, setBlobMD5] = useState("")

    const startRecording = () => {
        setRecored(true);
    }
    const stopRecording = () => {
        setRecored(false);
    }
    const onData = (recordedBlob) => {
        console.log('chunk of real-time data is: ', recordedBlob);
    }
    const onStop = (recordedBlob) => {
        console.log('recordedBlob is: ', recordedBlob);
        console.log(recordedBlob.blobURL);
        const blob = recordedBlob['blob'];
        const blobURL = recordedBlob.blobURL;
        setBlob(blob)
        //setBlobMD5(blob)
        setBlobURL(blobURL);
        setEverClickRecord(true);


    }
    const onPlay = () => {
        const sound = new Audio(blobURL);
        sound.play();
    }

    const onUpload = () => {
        //Convert blob to base64 string
        var reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function () {
            var base64String = reader.result.replace("data:audio/webm;codecs=opus;base64", "");

            //Post to Flask
            const post_value = {
                "ref_file": audioPath + '/' + audioName,
                "data": base64String,
            };

            axios.post(REST_AUDIO_CHECK_SERVER, post_value)
                .then(res => {
                    console.log(res);
                    console.log(res.data);

                    passLevel(res.data)
                });
        }
    }

    const [passMessage, setPassMessage] = useState()
    //SweetAlert Pass Level 
    const passLevel = (score) => {
        if (syllable == 5) {
            if (score > 11 && score < 55) {
                passed()
            } else {
                notPassed()
            }
        }else if (syllable == 4) {
            if (score > 11 && score < 50) {
                passed()
            } else {
                notPassed()
            }
        }else if (syllable == 3) {
            if (score >= 8 && score <= 30) {
                passed()
            } else {
                notPassed()
            }
        }else if (syllable == 2) {
            if (score >= 8 && score <= 25) {
                passed()
            } else {
                notPassed()
            }
        } else if (syllable == 1) {
            if (score >= 4 && score < 20) {
                passed()
            } else {
                notPassed()
            }
        }
        else {
            if (score > 8 && score < 50) {
                passed()
            } else {
                notPassed()
            }
        }

    }
    const passed = () => {

        swal({
            title: "Jadi sungoh !! ",
            text: "เยี่ยมจริง ๆ! ",
            icon: "success",
            buttons: ["Mula(อีกครั้ง)", "Wah hok la-in(ข้อถัดไป)"],
        }).then((result) => {
            if (result) {
                //next step and set default
                if (nextStep == wordDB.length - 1) {
                    Congratulation()
                    postDataNextLevelSetDefault()
                } else {
                    setNextStep(nextStep + 1);
                    setRecored(false);
                    setEverClickRecord(false);
                    postDataNextLevel()
                }
            } else {
                setRecored(false);
                setEverClickRecord(false);
            }
        });
    }

    const notPassed = () => {
        swal({
            title: "Tak lepah!... siki lagi",
            text: "ไม่ผ่านจ้า!... ลองอีกครั้งนะ ! ",
            icon: "error",
            buttons: "ยอ ๆ",
        }).then((result) => {
            setRecored(false);
            setEverClickRecord(false);
        });
    }
    //SweetAlert Congratulation
    const Congratulation = () => {
        swal({
            title: "ยินด้วย..คุณสำเร็จบทนี้แล้ว",
            text: "เยี่ยมจริง ๆ!",
            icon: "success",
            buttons: "ตกลง",
        }).then((res) => {
            //post data And Redirect to lesson page 
            postDataSuccessLesson()
            history.push("/learn")
        });
    }

    //Post data nextlevel On firebase.
    const postDataNextLevel = async () => {
        var userId = currentUser.uid;
        var updates = {};
        updates['/users/' + userId + '/successLearn/lesson' + lessonDbID + '/step'] = nextStep + 1;
        await firebase.database().ref().update(updates);
    }

    //If success lesson set nextlevel default 0.
    const postDataNextLevelSetDefault = async () => {
        var userId = currentUser.uid;
        var updates = {};
        updates['/users/' + userId + '/successLearn/lesson' + lessonDbID + '/step'] = 0;
        await firebase.database().ref().update(updates);
    }

    const postDataSuccessLesson = async () => {
        var userId = currentUser.uid;
        var updates = {};
        updates['/users/' + userId + '/successLearn/lesson' + lessonDbID + '/win'] = win + 1;
        await firebase.database().ref().update(updates);
    }

    /***************************************** EndMic ***************************************/

    return (
        <>
            {
                preLoading ? (
                    <>
                        <Lottie options={defaultOptions} height={200} width={200} />
                    </>
                ) : (
                        <>
                            <div className={classes.bodys}>
                                <Container maxWidth="lg" className={classes.root}>
                                    <Progressbar percentValue={nextStep * 100 / (wordDB.length - 1)} />
                                    <br />
                                    <h2 className={classes.topic}>Tae-ngok gama denga sorgor pahtu kaejeak wi beto</h2>
                                    <p className={classes.topic}>(ดูภาพและฟังเสียงแล้วพูดตามเสียงให้ถูกต้อง)</p>
                                    {/* Image */}
                                    <Grid container>
                                        <Grid item xs sm md lg className={classes.gcenter}>
                                            <Image imageName={category + '/' + word + '/' + imageFileName} />
                                        </Grid>
                                    </Grid>




                                    {/* Audio*/}
                                    <Grid container>
                                        <Grid item xs sm md lg className={classes.gcenter}>
                                            <Audios audioName={audioPath + '/' + audioName} />
                                        </Grid>
                                    </Grid>

                                    {/* Text box */}
                                    <Grid container>
                                        <Grid item xs sm md lg className={classes.gcenter}>
                                            <h3>ความหมาย: <p style={{ fontWeight: 'normal' }}>  {meaningLabel} </p> </h3>
                                        </Grid>
                                    </Grid>

                                    {/********************* Mic ***************************/}
                                    <Grid container>
                                        <Grid item xs sm md lg className={classes.gcenter}>
                                            <ReactMic
                                                record={record}
                                                className={classes.micVisual}
                                                onStop={onStop}
                                                onData={onData}
                                                strokeColor="#000000"
                                                backgroundColor="#FF4081" />

                                            {
                                                !record ? (
                                                    <>
                                                        {
                                                            !everClickPlaySound ? (
                                                                <Button variant="contained" disabled >
                                                                    <MicRounded /> Taka' sorgor (กดที่นี่เพื่ออัดเสียง)
                                                                </Button>
                                                            ) : (
                                                                    <Button variant="contained" onClick={startRecording} >
                                                                        <MicRounded /> Taka' sorgor mula (อัดเสียงอีกครั้ง)
                                                                    </Button>
                                                                )
                                                        }
                                                    </>

                                                ) : (
                                                        <Button variant="contained" onClick={stopRecording} >
                                                            <SettingsVoiceRounded /> wadu taka' (กดอีกครั้งเพื่อหยุด)
                                                        </Button>
                                                    )
                                            }

                                            {
                                                !record && everClickRecord && (
                                                    <>
                                                        <Button variant="contained"
                                                            onClick={onPlay}>
                                                            <RecordVoiceOverRounded /> denga sorgor (ฟังเสียง)
                                                        </Button>
                                                        {/*<Button variant="contained" color="primary"
                                                            onClick={onUpload}>
                                                            ตรวจสอบ
                                                        </Button>*/}
                                                    </>
                                                )
                                            }
                                        </Grid>
                                    </Grid>




                                </Container>
                                <div className={classes.footer}>
                                    <Container>
                                        {/* Button next, previous */}
                                        <Grid container>

                                            <Grid item xs sm md lg className={classes.gcenter}>
                                                <Button variant="contained" onClick={previous} >belong (ข้อก่อนหน้า)</Button>
                                            </Grid>

                                            {!record && everClickRecord ? (
                                                <Grid item xs sm md lg className={classes.gcenter}>
                                                    <Button variant="contained" color="primary" onClick={onUpload}>
                                                        paesa' (ตรวจสอบ)
                                                    </Button>
                                                </Grid>
                                            ) : (
                                                    <Grid item xs sm md lg className={classes.gcenter}>
                                                        <Button variant="contained" onClick={onUpload} disabled>
                                                            paesa' (ตรวจสอบ)
                                                        </Button>
                                                    </Grid>
                                                )}

                                        </Grid>
                                    </Container>
                                </div>
                            </div>
                        </>
                    )
            }



        </>

    );
}
export default LearnMode1;

//classes
const useStyles = makeStyles((theme) => ({
    bodys: {
        position: 'relative',
        width: '100%',
        backgroundColor: '#fff',
        minHeight: '100vh',
        overflow: 'hidden',
    },
    root: {
        flexGrow: 1,
    },
    gcenter: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    item: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    topic: {
        margin: 15,
        paddingLeft: '10%',
    },
    footer: {
        textAlign: 'left',
        maxWidth: '100%', /* ความสูงของ footer */
        display: 'block',
        padding: '15px',
        margin: '7px',
        borderTop: 'solid',
        borderWidth: '2px',
        borderColor: '#D1D0D0',

    },



    //Mic
    micVisual: {
        margin: 0,
        padding: 0,
        position: 'absolute',
        visibility: 'hidden',
    },
    button: {
        backgroundColor: '#5bbce4'
    }
}));
