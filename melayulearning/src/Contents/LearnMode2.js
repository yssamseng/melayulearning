import React from 'react';
import './LearnMode2.css';
//container
import Container from '@material-ui/core/Container';
import Progressbar from "../Component/Progressbar";
import Image from "../Component/Image";
import Microphone from "../Component/Microphone";

function LearnMode2() {

  <title>Image</title>
  return (
    <Container>
      <Progressbar />
      <div class="page-header">
        <h1>ภาพนี้เรียกว่า</h1>
      </div>
      <br></br>
      <div>
        <center>
          <Image />
          <Microphone/>
        </center>
      </div>
      <br/><br/><br/>
    </Container >

  );
}

export default LearnMode2;