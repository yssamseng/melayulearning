import React, { useRef, useState } from "react"
import { useHistory } from "react-router-dom"
import firebase from 'firebase/app';
import { useAuth } from "../Contexts/AuthContext"

import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core'
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles(theme => ({
    topic: {
        fontSize: '50px',
    }
}))

export default function Login() {
    const history = useHistory()
    const classes = useStyles();

    const emailRef = useRef()
    const passwordRef = useRef()
    const { login, currentUser } = useAuth()
    const [error, setError] = useState("")
    const [loading, setLoading] = useState(false)

    const handleLogin = async (e) => {
        e.preventDefault()
        try {
            setError("")
            setLoading(true)
            await login(emailRef.current.value, passwordRef.current.value)
            history.push("/learn")
        } catch {
            setError("Failed to log in")
        }
        setLoading(false)
    }
    const handleLoginWithGoogle = async () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        try {
            setError("")
            setLoading(true)
            await firebase.auth().signInWithPopup(provider)
            history.push("/learn")
        } catch (error) {
            setError("Failed to log in")
        }
        setLoading(false)
    }


    return (
        <>
            <div className="bodys">
                <div className="container">
                    <div className="forms-container">
                        <div className="signin-signup">
                            {error && <Alert variant="filled" severity="error">{error}</Alert>}
                            <form action="#" className="sign-in-form">
                                <h1 className={classes.topic}>
                                    KAEJAEK NAYU
                                </h1>
                                <h2 className="title">Masok Ngaji</h2>
                                <div className="input-field">
                                    <i className="fas fa-envelope"></i>
                                    <input type="email" placeholder="Email" ref={emailRef} />
                                </div>
                                <div className="input-field">
                                    <i className="fas fa-lock"></i>
                                    <input type="password" placeholder="Password" ref={passwordRef} />
                                </div>
                                <input type="submit" value="Masok" onClick={handleLogin} className="btn solid" />
                                <p className="social-text">Masok denga banci laing</p>
                                <div className="social-media">
                                    <a onClick={handleLoginWithGoogle} className="social-icon">
                                        <i className="fab fa-google"></i>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div className="panels-container">
                        <div className="panel left-panel">
                            <div className="content">
                                <h3>Wah banci baru</h3>
                                <button className="btn transparent" id="sign-up-btn" onClick={() => history.push('/signup')}>Wah banci</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
