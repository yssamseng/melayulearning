import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Grid, Button, Container, InputLabel, FormControl, Select } from '@material-ui/core';

//css
const useStyles = makeStyles((theme) => ({
    body: {
        position: 'relative',
        width: '100%',
        backgroundColor: '#fff',
        minHeight: '100vh',
        overflow: 'hidden',
    },

    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '500px',
            textAlign: 'center',
            position: 'relative',
            left: '360px',

        },
    },
    margin: {
        margin: theme.spacing(1),
        position: 'relative',
        left: '575px',
    },
    image: {
        width: 200,
        position: 'relative',
        left: '520px',

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        position: 'relative',
        left: '373px',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    font: {
        textAlign: 'center',
    },
}));

//main function
function Profile() {
    const classes = useStyles();
    const [state, setState] = React.useState({
        age: '',
        gender: '',
    });

    const handleChange = (event) => {
        const name = event.target.name;
        setState({
            ...state,
            [name]: event.target.value,
        });
    };

    return (
        <Container>
            <div className={classes.body}>
                <h3 className={classes.font}>โปรไฟล์</h3>
                <img src="./Picture/profile_icon.png" className={classes.image} alt="" />
                <form >
                    <div className={classes.root} noValidate autoComplete="off">
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={6} xs={12}>
                                <TextField label="ชื่อ" variant="outlined" />
                            </Grid>
                            <Grid item lg={6} md={6} sm={6} xs={12}>
                                <TextField label="นามสกุล" variant="outlined" />
                            </Grid>
                        </Grid>
                    </div>
                    <div className="age_birth">
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={6} xs={12}>
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel htmlFor="outlined-age-native-simple">Age</InputLabel>
                                    <Select
                                        native
                                        value={state.age}
                                        onChange={handleChange}
                                        label="Age"
                                        inputProps={{
                                            name: 'age',
                                            id: 'outlined-age-native-simple',
                                        }}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6} md={6} sm={6} xs={12}>
                                <form className={classes.container} noValidate>
                                    <TextField
                                        id="date"
                                        label="Birthday"
                                        type="date"
                                        defaultValue="2017-05-24"
                                        className={classes.textField}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                </form>
                            </Grid>
                        </Grid>
                    </div>
                    <div className="gender">
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel htmlFor="outlined-age-native-simple">Gender</InputLabel>
                            <Select
                                native
                                value={state.gender}
                                onChange={handleChange}
                                label="Gender"
                                inputProps={{
                                    name: 'gender',
                                    id: 'outlined-gender-native-simple',
                                }}
                            >
                                <option aria-label="None" value="" />
                                <option value={1}>male</option>
                                <option value={2}>female</option>
                            </Select>
                        </FormControl>
                    </div>
                    <div className={classes.root} noValidate autoComplete="off">
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={6} xs={12}>
                                <TextField label="เบอร์โทรศัพท์" variant="outlined" />
                            </Grid>
                        </Grid>
                    </div>
                    <div className={classes.margin} >
                        <Button variant="outlined" size="medium" color="primary" >บันทึก</Button>
                    </div>
                </form>
            </div>
        </Container >
    );
}

export default Profile;