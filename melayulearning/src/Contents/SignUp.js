import React, { useRef, useState } from "react"
//import { Form, Button, Card, Alert } from "react-bootstrap"
import { useAuth } from "../Contexts/AuthContext"
import { Link, useHistory } from "react-router-dom"
import firebase from 'firebase/app';

import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core'
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles(theme => ({
    topic: {
        fontSize: '50px',
    }
}))

export default function SignUp() {
    const classes = useStyles();

    const emailRef = useRef()
    const passwordRef = useRef()
    const passwordConfirmRef = useRef()
    const { signup, logout } = useAuth()
    const [error, setError] = useState("")
    const [loading, setLoading] = useState(false)
    const history = useHistory()

    async function handleSignUp(e) {
        e.preventDefault()

        if (passwordRef.current.value !== passwordConfirmRef.current.value) {
            return setError("รหัสผ่านไม่ตรงกัน")
        }

        /*await firebase.auth().fetchSignInMethodsForEmail(res.user.email)
                    .then(async providers => {
                        console.log('login index providers', providers.length)
                        if (provider.length == 1) {
                            history.push("/learn")
                        } else if (provider.length == 0){
                            await logout()
                            history.push("/login")
                        }
                    }).catch(function (error) {
                        console.log('login index providers error', error)
                    })*/

        try {
            setError("")
            setLoading(true)
            await signup(emailRef.current.value, passwordRef.current.value)
            history.push("/")
        } catch {
            setError("Failed to create an account")
        }

        setLoading(false)
    }
    return (
        <>
            <div className="body">
                <div className="container">
                    <div className="forms-container">
                        <div className="signin-signup">
                            <form onSubmit={handleSignUp} className="sign-in-form">
                                <h1 className={classes.topic}>KAEJAEK NAYU</h1>
                                <h2 className="title">Wa' banci baru</h2>
                                
                                <div className="input-field">
                                    <i className="fas fa-envelope"></i>
                                    <input type="email" placeholder="Email" ref={emailRef} />
                                </div>
                                <div className="input-field">
                                    <i className="fas fa-lock"></i>
                                    <input type="password" placeholder="Password" ref={passwordRef} />
                                </div>
                                <div className="input-field">
                                    <i className="fas fa-lock"></i>
                                    <input type="password" placeholder="Confirm Password" ref={passwordConfirmRef} />
                                </div>
                                <input type="submit" className="btn" value="Wa' Banci" />
                                {error && <Alert variant="filled" severity="error">{error}</Alert>}
                            </form>
                        </div>
                    </div>

                    <div className="panels-container">
                        <div className="panel left-panel">
                            <div className="content">
                                <h3>Masok Ngaji</h3>
                                <button className="btn transparent" id="sign-up-btn" onClick={()=>history.push('/login')}>Masok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
