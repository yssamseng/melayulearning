import React, { useRef, useState } from "react"
import { useHistory } from "react-router";
import { useAuth, updatePassword } from "../Contexts/AuthContext"

import { makeStyles } from '@material-ui/core/styles';
import { Button, Card, CardActions, CardContent, TextField, Grid, Container } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles({
    root: {
        position: 'relative',
        textAlign: 'center',
        width: '100%',
        backgroundColor: 'skyblue',
        minHeight: '100vh',
        overflow: 'hidden',
    },
    card: {
        maxWidth: '100%',
        margin: '7% 500px',
        textAlign: 'center',

    },
    form: {
        textAlign: 'center',
        '& div': {
            margin: '5px',
        },
        '& h3': {
            textAlign: 'left',
            minWidth: '20px',
            marginLeft: '8.8rem'
        }
    },
    title: {
        fontSize: 25,
        textAlign: 'center',
    },
    button: {
        textAlign: 'right',
        margin: '10px',
        display: 'flex',
        boxSizing: 'border-box',
        flexDirection: 'row',
        justifyContent: 'center',
        '& button': {
            marginLeft: '80px',
            marginRight: '80px',
        }
    },

});

export default function OutlinedCard() {
    const { currentUser, updatePassword, updateEmail } = useAuth()
    const classes = useStyles();
    const history = useHistory();

    //const emailRef = useRef()
    const passwordRef = useRef()
    const passwordConfirmRef = useRef()
    const [error, setError] = useState("")
    const [loading, setLoading] = useState(false)

    async function handleEdit(e) {
        e.preventDefault()
        if (passwordRef.current.value !== passwordConfirmRef.current.value) {
            return setError("Passwords do not match")
        }

        setLoading(true)
        setError("")

        /*if (emailRef.current.value !== currentUser.email) {
            promises.push(updateEmail(emailRef.current.value))
        }*/
        if (passwordRef.current.value) {
            await updatePassword(passwordRef.current.value).then(() => {
                history.push("/")
            }).catch(() => {
                setError("Failed to update account")
            }).finally(() => {
                setLoading(false)
            })
        }
    }


    const handleBack = () => {
        history.push('/learn')
    }


    return (
        <div className={classes.root}>

            <Card className={classes.card} variant="outlined">
                <CardContent>
                    <form className={classes.form}>
                        <h1>ACCOUNT</h1>
                        <div>
                            <TextField label="อีเมล" variant="outlined" value={currentUser.email} disabled />
                        </div>

                        <h3>เปลี่ยนรหัสผ่าน</h3>
                        <div>
                            <TextField type="password" label="รหัสผ่านใหม่" variant="outlined" autocomplete="off" ref={passwordRef} />
                        </div>
                        <div>
                            <TextField type="password" label="ยืนยันรหัสผ่าน" variant="outlined" autocomplete="off" ref={passwordConfirmRef} />
                        </div>
                        <div >

                        </div>
                        <div className={classes.button}>
                            <Button variant="outlined" color="primary" onClick={handleBack}>ย้อนกลับ</Button>
                            <Button variant="outlined" type="submit" color="secondary" onClick={handleEdit}>แก้ไข</Button>
                        </div>
                        {error && <Alert variant="filled" severity="error">{error}</Alert>}
                    </form>
                </CardContent>
            </Card>

        </div>

    );
}
