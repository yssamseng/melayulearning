import React from 'react';
import { useHistory } from "react-router-dom";

import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';



import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles(theme => ({
    main: {
        position: 'relative',
        width: '100%',
        backgroundColor: '#fff',
        minHeight: '100vh',
        overflow: 'hidden',
    },
    appBar: {
        backgroundColor: '#001231',
        '& p': {
            color: 'White',
        }
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },

    background: {
        position: 'relative',
        color: 'white',
        '& img': {
            width: '100%',
            height: '718.5px',
            objectFit: 'center',
            filter: 'brightness(50%)',
        }

    },
    text: {
        position: 'absolute',
        top: '28%',
        left: '10%',
        transform: 'translate(-50 %, -50 %)',
        textAlign: 'left',
        padding: 20,

        '& h1': {
            color: 'White',
            textTransform: 'uppercase',
            fontSize: '3em',
            opacity: 1,
        },
        ' & p': {
            color: 'White',
            textTransform: 'uppercase',
            fontSize: '1.2em',
            opacity: 1,
        },
        ' & button': {
            opacity: 1,
        },
    },

    button: {
        display: 'flex',
        boxSizing: 'border - box',
        //justifyContent: 'center',
    },

    buttonRed: {
        color: '#FE6B8B',
        borderColor: '#FE6B8B',
        borderWidth: 3,
        height: 48,
        padding: '0px 30px',
        margin: '0px 10px',
        '&:hover': {
            transition: '0.5s',
            background: 'linear-gradient(45deg, #FF8E53 30%, #FE6B8B 90%)',
            color: '#fff',
        },
    },
    buttonBlue: {
        color: '#21CBF3',
        borderColor: '#21CBF3',
        borderWidth: 3,
        height: 48,
        padding: '0px 30px',
        margin: '0px 10px',
        '&:hover': {
            transition: '0.5s',
            background: 'linear-gradient(45deg, #21CBF3 30%, #2196F3 90%)',
            color: '#fff',
        },
    },

}))

function Main_Menu() {
    const classes = useStyles();
    const history = useHistory();

    return (

        <div className={classes.main}>
            <AppBar className={classes.appBar}>
                <Container maxWidth="lg">
                    <Toolbar>
                        <Typography className={classes.title}>
                            <h2 onClick={() => history.push('/')}>KAEJAEK NAYU</h2>
                        </Typography>
                    </Toolbar>
                </Container>
            </AppBar>

            <div className={classes.background}>
                <img src="./Picture/background.jpg" alt="bg" />
                <div className={classes.text}>
                    <Container>
                        <h1>ยินดีต้อนรับ</h1>
                        <p>ระบบสนับสนุนการฝึกฝนภาษามลายูท้องถิ่นด้วยตนเอง</p>
                        <p>Local Melayu Language Self-Study Aid System</p>

                        <div className={classes.button}>
                            <Button variant="outlined" onClick={() => history.push('/signup')} className={classes.buttonRed}>
                                wa' banci
                            </Button>
                            <Button variant="outlined" onClick={() => history.push('/login')} className={classes.buttonBlue}>
                                Masok ngaji
                            </Button>
                        </div>
                    </Container>
                </div>
            </div>
        </div>
    );
}
export default Main_Menu;