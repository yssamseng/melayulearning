import React, { useState, useEffect } from 'react';
import firebase from "firebase";
import { useAuth } from "../Contexts/AuthContext"

import { makeStyles } from '@material-ui/core/styles';

import { Container, Grid, Button } from '@material-ui/core';

import Lottie from "react-lottie";
import * as loadingData from "../loading.json";

import Navbar from '../Component/Navbar';
import Egg from "../Component/Egg";

const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        width: '100%',
        backgroundColor: '#fff',
        minHeight: '100vh',
        overflow: 'hidden',
    },
    LearnLessons: {
        margin: 100,
        textAlign: 'center',
        '& h5': {
            padding: 2,
            margin: 0,
        },

    },
    title:{
        fontSize: '24px',
        fontWeight: 550,
    },

    

    loading: {
        margin: '500px'
    }
}))

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loadingData.default,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
    }
};


function DbLerning() {
    const { currentUser } = useAuth()

    const classes = useStyles();
    const [preLoading, setPreloading] = useState(true)
    const [lessonSuccess, setLessonSuccess] = useState([])

    const jsonDB = [
        { lessonDbID: 1, topic: "พื้นฐานการทักทาย", jsonDBname: "DB_introduction" },
        { lessonDbID: 2, topic: "การเดินทาง", jsonDBname: "DB_traveling" },
        { lessonDbID: 3, topic: "สถานที่", jsonDBname: "DB_introduction" },
        { lessonDbID: 4, topic: "ร้านอาหาร", jsonDBname: "DB_traveling" },
        { lessonDbID: 5, topic: "พื้นฐานการทักทาย", jsonDBname: "DB_introduction" },
        { lessonDbID: 6, topic: "การเดินทาง", jsonDBname: "DB_traveling" },
    ]

    const updateData = async () => {
        var userId = /*firebase.auth().*/currentUser.uid;
        var win = []
        for (var i = 1; i <= 3; i++) {
            await firebase.database().ref('/users/' + userId + '/successLearn/lesson' + i).once('value').then((snapshot) => {
                win[i] = (snapshot.val() && snapshot.val().win) || 0;
            });
        }
        setLessonSuccess(win)
        //set preload false
        setPreloading(false)
    }

    useEffect(() => {
        updateData()
    }, [])


    return (
        <>
            {
                preLoading ? (
                    <>
                        <Lottie options={defaultOptions} height={200} width={200}/>
                    </>
                ) : (
                        <>
                            <div className={classes.root}>
                            <Navbar />
                            <Container maxWidth="lg">
                                <div className={classes.LearnLessons}>
                                    <h1 className={classes.title}>Pileh hoh nok Ngaji (เลือกบทเรียนที่สนใจ)</h1>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12} sm={12} md={12} lg={12} >
                                            <Egg lessonDbID={1} numCongratulation={lessonSuccess[1]} />
                                            <h5>Mudoh-Mudoh</h5>
                                            <h5> {jsonDB[0].topic} </h5>
                                        </Grid>
                                        <Grid item xs={12} sm={6} md={6} lg={6}>
                                            <Egg lessonDbID={2} numCongratulation={lessonSuccess[2]} />
                                            <h5>Jalae</h5>
                                            <h5> {jsonDB[1].topic} </h5>
                                        </Grid>
                                        <Grid item xs={12} sm={6} md={6} lg={6}>
                                            <Egg lessonDbID={3} numCongratulation={lessonSuccess[3]} />
                                            <h5>Tepat</h5>
                                            <h5> {jsonDB[2].topic} </h5>
                                        </Grid>
                                    </Grid>

                                </div>
                            </Container>
                            </div>
                        </>
                    )
            }
        </>
    );
}
export default DbLerning;