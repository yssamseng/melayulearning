
import React, { useEffect, useState } from "react";
import { ReactMic } from "react-mic";

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import MicRoundedIcon from '@material-ui/icons/MicRounded';
import SettingsVoiceRoundedIcon from '@material-ui/icons/SettingsVoiceRounded';
import RecordVoiceOverRoundedIcon from '@material-ui/icons/RecordVoiceOverRounded';

import swal from 'sweetalert';
import axios from 'axios';
import md5 from 'md5';

const useStyles = makeStyles(theme => ({
  micVisual: {
    margin: 0,
    padding: 0,
    position: 'absolute',
    visibility: 'hidden',
  },
  button: {
    backgroundColor: '#5bbce4'
  },
  gcenter: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },

}))

let x;


function Microphone() {
  const classes = useStyles();

  const [record, setRecored] = useState(false);
  const [blob, setBlob] = useState("");
  const [blobURL, setBlobURL] = useState("");
  //const [blobMD5, setBlobMD5] = useState("")

  const [everClick, setEverClick] = useState(false)

  x = blobURL;
  

  const startRecording = () => {
    setRecored(true);
  }
  const stopRecording = () => {
    setRecored(false);
  }
  const onData = (recordedBlob) => {
    console.log('chunk of real-time data is: ', recordedBlob);
  }
  const onStop = (recordedBlob) => {
    console.log('recordedBlob is: ', recordedBlob);
    console.log(recordedBlob.blobURL);
    const blob = recordedBlob;
    const blobURL = recordedBlob.blobURL;
    setBlob(blob)
    //setBlobMD5(blob)
    setBlobURL(blobURL);
    setEverClick(true);


  }

  const onPlay = () => {
    const audio = new Audio(blobURL);
    audio.play();
  }
  const onUpload = () => {
    const audio = new Audio(blobURL);
    swal({
      title: "ยาดีสูโงห!..คุณทำได้ 80 คะแนน",
      text: "เยี่ยมจริง ๆ! ",
      icon: "success",
      buttons: ["ทำอีกครั้ง", "ไปข้อต่อไป"],
    });

    axios.get('http://localhost:8080/api/v1/hello/1')
      .then((response) => {
        console.log(response.data)
      })


    let URL = 'http://localhost:8080/test'

    let form = [
      {
        "rawFile": {
          "preview": blob
        },
        "src": blobURL,
        "title": "newUp.webm"
      }
    ]

    let formData = new FormData()
    console.log(blobURL)
    formData.append('file', (blob))


    axios.post(URL, formData).then(response => {
      alert(response.data)
    }).catch(error => {
      console.log('error', error)
    })
  }




  return (
    <>
      <ReactMic
        record={record}
        className={classes.micVisual}
        onStop={onStop}
        onData={onData}
        strokeColor="#000000"
        backgroundColor="#FF4081"
      />

      <Grid container>
        <Grid item xs sm md lg className={classes.gcenter}>

        </Grid>
        <Grid item xs sm md lg className={classes.gcenter}>
          {
            !record ? (
              <Button variant="contained" onClick={startRecording} >
                <MicRoundedIcon /> กดที่นี่เพื่ออัดเสียง
              </Button>
            ) : (
                <Button variant="contained" onClick={stopRecording} >
                  <SettingsVoiceRoundedIcon /> กำลังอัดเสียง.. กดอีกครั้งเพื่อหยุด
                </Button>
              )
          }

        </Grid>
        <Grid item xs sm md lg className={classes.gcenter}>
          {
            !record && everClick && (
              <Button variant="contained" onClick={onPlay}>
                <RecordVoiceOverRoundedIcon /> ฟังเสียง
              </Button>
            )
          }
        </Grid>
        <Grid item xs sm md lg className={classes.gcenter}>
          {
            !record && everClick && (
              <div className={classes.checkSound}>
                <Button variant="contained" color="primary" onClick={onUpload}>
                  ตรวจสอบ
            </Button>
              </div>

            )
          }
        </Grid>
      </Grid>









      {/*<audio src={blobURL} controls />*/}

    </>
  );
}
//export default Microphone;

export { Microphone, x };