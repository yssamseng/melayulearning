import { React, useState } from "react";
import { makeStyles, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import IconButton from '@material-ui/core/IconButton';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';

import Grid from '@material-ui/core/Grid';
import { Container, useMediaQuery } from '@material-ui/core';
import { Link } from "react-router-dom";


const BorderLinearProgress = withStyles((theme) => ({
    root: {
        height: 12,
        borderRadius: 8,
    },
    colorPrimary: {
        backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
        borderRadius: 8,
        backgroundColor: '#1a90ff',
    },
}))(LinearProgress);


const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
    close: {
        '@media screen and (max-width:1780px)': {
            maxWidth: 22,
            height: 'auto',
            position: 'absolute',
            left: 250,
            top: 16.5,
        },
        '@media screen and (max-width:1500px)': {
            maxWidth: 22,
            height: 'auto',
            position: 'absolute',
            left: 195,
            top: 16.5,
        },
        '@media screen and (max-width:1200px)': {
            maxWidth: 22,
            height: 'auto',
            position: 'absolute',
            left: 120,
            top: 17.5,
        },
        '@media screen and (max-width:1024px)': {
            maxWidth: 19,
            height: 'auto',
            position: 'absolute',
            left: 110,
            top: 19,
        },
        '@media screen and (max-width:768px)': {
            maxWidth: 18,
            height: 'auto',
            position: 'absolute',
            left: 87,
            top: 19.5,
        },
        '@media screen and (max-width:540px)': {
            maxWidth: 18,
            height: 'auto',
            position: 'absolute',
            left: 15,
            top: 67.5,
        },
        '@media screen and (max-width:480px)': {
            maxWidth: 18,
            height: 'auto',
            position: 'absolute',
            left: 15,
            top: 67.5,
        },

    },

});


function Progressbar({ percentValue = 80 }) {
    const classes = useStyles();

    return (
        <Container maxWidth="lg">
            <div className={classes.root}>
                <br />
                <Grid container spacing={6}>
                    <Grid item xs={1} sm={1} md={1} lg={1}>
                        <Link to="/learn">
                            <svg className={classes.close} viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g id="close 1">
                                    <path id="Vector" d="M15.96 12.0008L23.3847 4.57604C24.2051 3.75569 24.2051 2.42569 23.3847 1.60661L22.3949 0.616804C21.5743 -0.203807 20.2443 -0.203807 19.4252 0.616804L12.0008 8.04127L4.57604 0.615266C3.75569 -0.205089 2.42569 -0.205089 1.60661 0.615266L0.615266 1.60508C-0.205089 2.42569 -0.205089 3.75569 0.615266 4.57476L8.04127 12.0008L0.616804 19.4252C-0.203807 20.2458 -0.203807 21.5758 0.616804 22.3949L1.60661 23.3847C2.42697 24.2051 3.75697 24.2051 4.57604 23.3847L12.0008 15.96L19.4252 23.3847C20.2458 24.2051 21.5758 24.2051 22.3949 23.3847L23.3847 22.3949C24.2051 21.5743 24.2051 20.2443 23.3847 19.4252L15.96 12.0008Z" fill="#C5C5C5" />
                                </g>
                            </svg>

                        </Link>
                    </Grid>
                    <Grid item xs={11} sm={11} md={11} lg={11}>
                        <BorderLinearProgress variant="determinate" value={percentValue} />
                    </Grid>
                </Grid>


            </div>
        </Container>

    );
}
export default Progressbar;