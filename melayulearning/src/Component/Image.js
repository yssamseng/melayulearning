import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    image: {
        maxWidth: '400px',
        height: '225px',
        objectFit: 'cover',
        borderRadius:'4px',
    }
    
  }))
function Image({imageName="a03.jpg"}) {
    const classes = useStyles();

    const imageUrl = "../Word/" + imageName;

    const demo = "../Picture/a03.jpg";

    return(
        <img src={imageUrl} alt="Image learning" className={classes.image}></img>
    );
}
export default Image;