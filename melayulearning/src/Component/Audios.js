import React, { useState, useEffect } from "react";
import useSound from 'use-sound';
import Button from '@material-ui/core/Button';
import VolumeUpRoundedIcon from '@material-ui/icons/VolumeUpRounded';
import SurroundSoundRoundedIcon from '@material-ui/icons/SurroundSoundRounded';


function Audios({ audioName = 'audio01.wav' }) {

    const soundUrl = "../Word/" + audioName;

    const [play, { stop, isPlaying }] = new useSound(soundUrl);

    return (
        <div>
            {
                !isPlaying ? (
                    <>
                        <Button variant="contained" color="primary" onClick={() => play()} >
                            <VolumeUpRoundedIcon /> denga sorgor (กดที่นี่เพื่อฟังเสียง)
                        </Button>
                        
                    </>
                ) : (
                        <Button variant="contained" color="primary" >
                            <SurroundSoundRoundedIcon /> denga...
                        </Button>
                    )
            }
        </div>
    );
}
export default Audios;

/*
const useAudio = url => {
    const [audio] = useState(new Audio(url));
    const [playing, setPlaying] = useState(false);

    const toggle = () => setPlaying(!playing);

    useEffect(() => {
        playing ? audio.play() : audio.pause();
    },
        [playing]
    );

    useEffect(() => {
        audio.addEventListener('ended', () => setPlaying(false));
        return () => {
            audio.removeEventListener('ended', () => setPlaying(false));
        };
    }, []);

    return [playing, toggle];
};

const Player = ({ audioName = 'audio01.wav' }) => {
    const audioUrl = "../Word/" + audioName;
    const z = '../Word/audio01.wav';
    //const [playing, toggle] = useAudio(audioUrl);
    const [playing, toggle] = useAudio(audioUrl);

    const x = false
    return (

        <div>
            <button onClick={toggle}>{playing ? "Pause" : "Play"}</button>
        </div>
    );
};

export default Player;*/

