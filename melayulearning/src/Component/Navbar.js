import React from 'react';
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../Contexts/AuthContext"

import { makeStyles } from '@material-ui/core/styles';

import { AppBar, Toolbar, Typography, useMediaQuery, Container, IconButton, MenuIcon, Grid } from '@material-ui/core';
import { Button, Menu, MenuItem } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        '& a': {
            textDecoration: 'none',
        }
    },
    appBar: {
        backgroundColor: 'white',
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },

    button_icon: {
        padding: 1,
        paddingRight: 20,
        paddingLeft: 20,
    },
    iconImg: {
        padding: 1,
        width: 35,
        maxWidth: 100,
        margin: 10,
    },
}));

const Navbar = () => {
    const classes = useStyles();
    const mobileQuery = useMediaQuery('(min-width:480px)');

    const { currentUser, logout } = useAuth()
    const history = useHistory()

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleProfile = () => {
        setAnchorEl(null);
        history.push("/profile")
    }
    const handleMyAccount = () => {
        setAnchorEl(null);
        history.push("/account")
    }

    const handleLogOut = async () => {
        setAnchorEl(null); 
        {
            await logout()
            history.push("/")
        }
    }
    

    return (
        <div className={classes.root}>
            <AppBar className={classes.appBar}>
                <Container><Container>
                    <Toolbar>
                        <Typography className={classes.title}>
                            <Link to="/learn" >
                                <Button className={classes.button_icon}>

                                    <img className={classes.iconImg} src="./icon/languageLearning.png" alt="icon Language Learning" />
                                    {
                                        mobileQuery && (
                                            <h5>Ngaji</h5>
                                        )
                                    }

                                </Button>
                            </Link>
                            {/*<Link to="/message" >
                                <Button className={classes.button_icon}>

                                    <img className={classes.iconImg} src="./icon/icon-message.png" alt="icon Language Learning" />
                                    {
                                        mobileQuery && (
                                            <h5>กล่องข้อความ</h5>
                                        )
                                    }

                                </Button>
                                </Link>*/}

                        </Typography>

                        {/* icon profile */}

                        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} className={classes.button_icon}>
                            <img className={classes.iconImg} src="./icon/icon-profile.png" alt="icon Language Learning" />
                        </Button>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleProfile}>Profile</MenuItem>
                            <MenuItem onClick={handleMyAccount}>Banci Saya</MenuItem>
                            <MenuItem onClick={handleLogOut}>Tubek</MenuItem>
                        </Menu>

                    </Toolbar>
                </Container></Container>
            </AppBar>
        </div>
    );
}
export default Navbar;