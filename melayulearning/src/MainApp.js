import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

//Manage
import { AuthProvider } from "./Contexts/AuthContext"
import PrivateRoute from "./Component/PrivateRoute"
import PublicRoute from "./Component/PublicRoute"
import Home from './Contents/Home';
import Login from './Contents/Login'
import SignUp from './Contents/SignUp';
import LearnMode1 from './Contents/LearnMode1';
import Learn from './Contents/LearnLessons'
import Profile from "./Contents/Profile";
import Account from "./Contents/Account"
import Progress from "./Contents/Progress"

function MainApp() {
  return (
        <Router>
          <AuthProvider>
            <Switch>
              <PublicRoute exact path="/" component={Home} />
              <PublicRoute path="/login" component={Login} />
              <PublicRoute path="/signup" component={SignUp} />

              <PrivateRoute path="/learn" component={Learn} />
              <PrivateRoute path="/learningM1/:lessonDbID" component={LearnMode1} />
              <PrivateRoute path="/profile" component={Profile} />
              <PrivateRoute path="/account" component={Account} />
              <PrivateRoute path="/progress" component={Progress} />
            </Switch>
          </AuthProvider>
        </Router>
  )
}

export default MainApp
