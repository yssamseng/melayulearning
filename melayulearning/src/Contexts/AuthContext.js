import React, { useContext, useState, useEffect } from "react"
import firebase from "firebase";
import { auth } from "../firebase"

const AuthContext = React.createContext()

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState({})

  const [win, setWin] = useState([])

  const [loading, setLoading] = useState(true)

  function signup(email, password) {
    return auth.createUserWithEmailAndPassword(email, password)
  }

  function login(email, password) {
    return auth.signInWithEmailAndPassword(email, password)
  }

  function logout() {
    return auth.signOut()
  }

  function resetPassword(email) {
    return auth.sendPasswordResetEmail(email)
  }

  function updateEmail(email) {
    return currentUser.updateEmail(email)
  }

  function updatePassword(password) {
    return currentUser.updatePassword(password)
  }
  const updateDataLearnLesson = async() => {
    var winn = []
    for (var i = 0; i < 3; i++) {
      await firebase.database().ref('/users/' + currentUser.uid + '/successLearn/lesson' + i).once('value').then((snapshot) => {
        winn[i] = (snapshot.val() && snapshot.val().win) || 0;
      });
    }
    setWin(winn)
  }

  useEffect(() => {
    
    const fetchData = async () => {
      const unsubscribe = await auth.onAuthStateChanged(user => {
        //console.log(user)
        setCurrentUser(user)
        setLoading(false)
      });
      return unsubscribe
    }
    fetchData();
  }, [])

  const value = {
    currentUser,
    win,
    login,
    signup,
    logout,
    resetPassword,
    updateEmail,
    updatePassword,
    
  }

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
