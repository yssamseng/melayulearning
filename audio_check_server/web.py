from flask import Flask, request
from flask_cors import CORS, cross_origin

import base64

import librosa
from numpy.linalg import norm
from dtw import dtw

app = Flask(__name__)
cors = CORS(app)



def audio_compare (src_file, ref_file):
    y, sr = librosa.load(src_file)
    y2, sr2 = librosa.load(ref_file)
    print (src_file)
    print (ref_file)
    y_rms = librosa.feature.rms(y=y,  frame_length=128, hop_length=64)
    y2_rms = librosa.feature.rms(y=y2, frame_length=128, hop_length=64)
    da = dtw(y_rms, y2_rms)
    return str(da.distance)
 



@app.route('/')
def index():
    return 'Web App with Python Flask!'

@app.route('/eval_audio/' , methods=['GET', 'POST'])
@cross_origin()
def eval_audio():
    if request.method == 'POST':
        json_data = request.get_json()
        #print (json_data)
        base64_audio_bytes = json_data.get("data").encode('utf-8')
        ref_file =  json_data.get("ref_file")  
        print (ref_file)
        
       
        with open('tmp.webm', 'wb') as file_to_save:
            decoded_audio_data = base64.decodebytes(base64_audio_bytes)
            file_to_save.write(decoded_audio_data)
 
        return (audio_compare ("tmp.webm", ref_file))
       
    
app.run(host='0.0.0.0', port=8002)
